import numpy as np
cimport numpy as np
cimport cython
from cython.parallel cimport parallel, prange

cdef extern from "cppblocktree.h":
    cdef struct rect3d:
        double xmin
        double ymin
        double zmin
        double xmax
        double ymax
        double zmax

    cdef cppclass amr_tree:
        amr_tree(rect3d extent, size_t Nx, size_t Ny, size_t Nz) nogil except +
        size_t addblock(size_t index, double x, double y, double z, size_t node_type, size_t node_level) nogil except +
        size_t findblock(double x, double y, double z) nogil except +
        size_t findblock(double x, double y, double z, size_t maxlevel) nogil except +

cdef class Py_amr_tree:
    cdef amr_tree * c_amr_tree

    def __cinit__(self, double xmin, double ymin, double zmin, double xmax,
    double ymax, double zmax, size_t Nx, size_t Ny, size_t Nz):
        cdef rect3d extent
        extent.xmin = xmin
        extent.ymin = ymin
        extent.zmin = zmin
        extent.xmax = xmax
        extent.ymax = ymax
        extent.zmax = zmax
        self.c_amr_tree = new amr_tree(extent, Nx, Ny, Nz)
        
    def __dealloc__(self):
        del self.c_amr_tree

    cdef size_t addblock(self, size_t index, double x, double y, double z, size_t node_type, size_t node_level) nogil:
        return self.c_amr_tree.addblock(index, x, y, z, node_type, node_level)
    
    cdef size_t findblock(self, double x, double y, double z) nogil:
        return self.c_amr_tree.findblock(x, y, z)
        
    cdef size_t findblock_lmax(self, double x, double y, double z, size_t lmax) nogil:
        return self.c_amr_tree.findblock(x, y, z, lmax)
        
cdef Py_amr_tree Tree

@cython.wraparound(False)
@cython.boundscheck(False)
@cython.cdivision(True)
def Build(np.ndarray[np.float64_t, ndim=3] block_bbox, np.ndarray[np.uint64_t, ndim=1] nodetype):
    global Tree
    cdef size_t i
    cdef size_t N = len(block_bbox)

    cdef np.ndarray[np.float64_t, ndim=2] block_coord = .5*(block_bbox[:,:,0]+block_bbox[:,:,1])
    cdef np.ndarray[np.float64_t, ndim=2] block_len = block_bbox[:,:,1]-block_bbox[:,:,0]
    cdef np.ndarray[np.float64_t, ndim=1] block_len_max = np.max(block_len, axis=0)
    cdef np.ndarray[size_t, ndim=1] nodelevel = (np.log2(block_len_max[0]/block_len[:,0]) + 1.5).astype(np.uint64)

    cdef np.float64_t xmin = np.min(block_bbox[:,0,0])
    cdef np.float64_t ymin = np.min(block_bbox[:,1,0])
    cdef np.float64_t zmin = np.min(block_bbox[:,2,0])
    cdef np.float64_t xmax = np.max(block_bbox[:,0,1])
    cdef np.float64_t ymax = np.max(block_bbox[:,1,1])
    cdef np.float64_t zmax = np.max(block_bbox[:,2,1])

    cdef size_t Nx = np.uint64((xmax-xmin)/block_len_max[0]+.5)
    cdef size_t Ny = np.uint64((ymax-ymin)/block_len_max[1]+.5)
    cdef size_t Nz = np.uint64((zmax-zmin)/block_len_max[2]+.5)
      
    # Grundstein fuer Baum legen:
    Tree = Py_amr_tree(xmin, ymin, zmin, xmax, ymax, zmax, Nx, Ny, Nz)
    # Baum zusammenfuegen:
    for i in range(N):
        Tree.addblock(i, block_coord[i,0], block_coord[i,1], block_coord[i,2], nodetype[i], nodelevel[i])

    return block_coord, nodelevel, Nx, Ny, Nz

@cython.wraparound(False)
@cython.boundscheck(False)
cdef Findblock_c(np.ndarray[np.float64_t, ndim=1] x, np.ndarray[np.float64_t, ndim=1] y, np.ndarray[np.float64_t, ndim=1] z):
    global Tree
    cdef int i
    cdef int N = len(x)
    cdef np.ndarray[size_t, ndim=1] block = np.empty(N, dtype=np.uint64)
    with nogil, parallel():
        for i in prange(N, schedule='guided'):
            block[i] = Tree.findblock(x[i], y[i], z[i])  
    return block
    
@cython.wraparound(False)
@cython.boundscheck(False)
cdef Findblock_lmax_c(np.ndarray[np.float64_t, ndim=1] x, np.ndarray[np.float64_t, ndim=1] y, np.ndarray[np.float64_t, ndim=1] z, size_t lmax):
    global Tree
    cdef int i
    cdef int N = len(x)
    cdef np.ndarray[size_t, ndim=1] block = np.empty(N, dtype=np.uint64)
    with nogil, parallel():
        for i in prange(N, schedule='guided'):
            block[i] = Tree.findblock_lmax(x[i], y[i], z[i], lmax)
    return block

@cython.wraparound(False)
@cython.boundscheck(False)
def Findblock(x, y, z):
    global Tree
    cdef np.ndarray[np.float64_t, ndim=1] xflat = x.flatten()
    cdef np.ndarray[np.float64_t, ndim=1] yflat = y.flatten()
    cdef np.ndarray[np.float64_t, ndim=1] zflat = z.flatten()
    cdef np.ndarray[size_t, ndim=1] blocklist
    blocklist = Findblock_c(xflat, yflat, zflat)
    return blocklist.reshape([(x.shape)[i] for i in range(x.ndim)])
    
@cython.wraparound(False)
@cython.boundscheck(False)
def Findblock_lmax(x, y, z, size_t lmax):
    global Tree
    cdef np.ndarray[np.float64_t, ndim=1] xflat = x.flatten()
    cdef np.ndarray[np.float64_t, ndim=1] yflat = y.flatten()
    cdef np.ndarray[np.float64_t, ndim=1] zflat = z.flatten()
    cdef np.ndarray[size_t, ndim=1] blocklist
    blocklist = Findblock_lmax_c(xflat, yflat, zflat, lmax)
    return blocklist.reshape([(x.shape)[i] for i in range(x.ndim)])
