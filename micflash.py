# -*- coding: utf-8 -*-
import h5py
import numpy as np
from itertools import izip
from scipy.spatial import cKDTree
import sys
import blocktree

__author__ = "Michael Weis"
__version__ = "23f"

_verbose_ = True

# ==== HELPER ==================================================================
def rangeselect(L, L_min, L_max):
    a = np.array(L)
    mask = (a>=L_min)*(a<L_max)
    return a[mask]

def verboseprint(*args):
    if _verbose_:
        for arg in args:
            print(str(arg)),
        print
        sys.stdout.flush()

# ==== CLASS: flash file data reader ===========================================
def sanitize_dtype(dataset):
    """
    Sanitize the dtype of the provided dataset into datatypes of decent precision.
    This is necessary because of the precision of the variables stored
    in flash code plotfiles being typically reduced to save disk space,
    which may compromise the numerical precision of subsequent data
    processings if not sanitized.
    """
    if dataset.dtype == np.float16:
        data_cp = np.copy(np.array(dataset, dtype=np.float64))
    elif dataset.dtype == np.float32:
        data_cp = np.copy(np.array(dataset, dtype=np.float64))
    elif dataset.dtype == np.uint8:
        data_cp = np.copy(np.array(dataset, dtype=np.uint64))
    elif dataset.dtype == np.uint16:
        data_cp = np.copy(np.array(dataset, dtype=np.uint64))
    elif dataset.dtype == np.uint32:
        data_cp = np.copy(np.array(dataset, dtype=np.uint64))
    else:
        data_cp = np.copy(np.array(dataset))
    return data_cp        

class flashfile(object):
    def __init__(self, file_name):
        self.__filename = file_name
        self.__plotfile = h5py.File(file_name, 'r')
        self.__nodetype = sanitize_dtype(self.__plotfile['node type'])
    def __del__(self):
        self.__plotfile.close()
    def __repr__(self, level=0):
        answer  = 'HDF5 Flashfile' + '\n'
        answer += self.__filename+'\n'
        for key, dataset in self.__plotfile.iteritems():
            answer += '  '*level
            answer += str(dataset)+'\n'
        return answer
    def read_param(self, type_key, var_key):
        parameters = self.__plotfile[type_key]
        if parameters.dtype.names[0] == 'name':
            param_dict = dict([[key.rstrip(), val] for key, val in parameters])
        elif parameters.dtype.names[1] == 'name':
            param_dict = dict([[key.rstrip(), val] for val, key in parameters])
        else:
            raise IOError
        return np.copy(param_dict[var_key])
    def read_plotvar(self, plot_var, sanitize=True, leavesonly=True):
        if sanitize:
            dataset = sanitize_dtype(self.__plotfile[plot_var])
        else:
            dataset = self.__plotfile[plot_var]
        if leavesonly:
            return np.array(dataset)[self.__nodetype==1]
        else:
            return dataset
    def read(self, path=''):
        dataset = self.__plotfile
        route = filter(lambda subdir: subdir!='', path.split('/'))
        for subdir in route:
            dataset = dataset[subdir]
        return dataset
    def list(self, path=''):
        dataset = self.__plotfile
        route = filter(lambda subdir: subdir!='', path.split('/'))
        if len(route) == 0:
            return list(dataset.iterkeys())
        else:
            for subdir in route:
                dataset = dataset[subdir]
            return [param['name'].rstrip() for param in dataset]
    def filename(self):
        return self.__filename
    def search_param(self, key_str):
        for key, dataset in self.__plotfile.iteritems():
            if 'parameters' in key or 'scalars' in key:
                for parameter in dataset:
                    if key_str in parameter['name']:
                        print '----------------------------------------'
                        print key
                        print parameter['name'].rstrip()+':\t',
                        print parameter['value']

# ==== PARAMETER READER ========================================================
def ff_lmax(ffile, default=None):
    try:
        ljeans_max = ffile.read_param('integer runtime parameters', 'lrefine_max_jeans')
        lstd_max = ffile.read_param('integer runtime parameters', 'lrefine_max')
        refine_max = max(ljeans_max, lstd_max)
    except:
        try:
            refine_max = ffile.read_param('integer runtime parameters', 'lrefine_max')
        except:
            refine_max = default
    return refine_max
    
def ff_usegrav(ffile, default=True):
    try:
        sim_gravity = ffile.read_param('logical runtime parameters', 'usegravity')
    except:
        # Workaround for h5py bug not reading 'logical runtime parameters' in 2.60:
        try:
            gpot = ffile.read_plotvar('gpot')
            sim_gravity = ( gpot.min() != gpot.max() )
        except:
            sim_gravity = default
    return sim_gravity
    
def ff_coords(ffile):
    ### Get metrics of the bounding boxes of the blocks
    block_bbox = ffile.read_plotvar('bounding box')
    bbox_min = block_bbox.min(axis=2)
    bbox_max = block_bbox.max(axis=2)
    bbox_center = .5*(bbox_min+bbox_max)
    bbox_size = bbox_max - bbox_min
    ### Calculate coordinates of the cells inside the blocks
    nxb = ffile.read_param('integer scalars', 'nxb')
    nyb = ffile.read_param('integer scalars', 'nyb')
    nzb = ffile.read_param('integer scalars', 'nzb')
    x_reloffset = (np.linspace(-.5, .5, nxb, endpoint=False) + .5/nxb)
    y_reloffset = (np.linspace(-.5, .5, nyb, endpoint=False) + .5/nyb)
    z_reloffset = (np.linspace(-.5, .5, nzb, endpoint=False) + .5/nzb)
    reloffset = np.array(np.meshgrid(y_reloffset, z_reloffset, x_reloffset))[(2,0,1),:,:,:]
    offset = (bbox_size).reshape(-1,3,1,1,1)*reloffset
    coords = (bbox_center).reshape(-1,3,1,1,1) +offset
    return coords

# ==== CLASS: flash file chemistry manager =====================================
k_b = 1.38064852e-16    # Boltzmann constant (erg/K)
m_a = 1.660539040e-24     # atomic mass unit (g)/(Da)

ch_mu15 = {'Ha': 1.008, 'He': 4.002602, 'C': 12.011, 'O': 15.9994, 'Si': 28.0855,
    'H2': 2.01588, 'Hp': 1.0072, 'Hep': 4.002602, 'CHx': 13., 'OHx': 17., 
    'Cp': 12.011, 'HCOp': 29., 'CO': 28.01, 'Mp': 28., 'M':28.}

ch_mu5 = {'Ha': 1.0, 'He': 4.002602, 'C': 12.011, 'O': 15.9994, 'Si': 28.0855,
    'H2': 2.0, 'Hp': 1.0, 'Cp': 12.011, 'CO': 28.01,}
      
ch_ikey = {'Ha': 'iha ', 'He': 'ihe ', 'C': 'ic  ', 'O': 'io  ', 'Si': None,
    'H2': 'ih2 ', 'Hp': 'ihp ', 'Hep': 'ihep', 'CHx': 'ichx', 'OHx': 'iohx',
    'Cp': 'icp ', 'HCOp': 'ihco', 'CO': 'ico ', 'Mp': 'imp ', 'M':'im  '}
ch_ikey_r = dict((val, key) for key, val in ch_ikey.iteritems())

ch_i5 = ['Ha', 'Hp', 'H2', 'CO', 'Cp']
    
class ffchem(object):
    def __init__(self, ffile, network=None):
        self.__ffile = ffile
        self.__dens = self.__ffile.read_plotvar('dens')
        # Compare species keys present in plotfile to the known ones:
        ffile_iX = set(ch_ikey.values()).intersection(self.__ffile.list())
        self.__species = [ ch_ikey_r[item] for item in ffile_iX ]
        # Try to guess the correct chemical network, if none provided:
        if network is None:
            if not set(self.__species).issubset(ch_ikey.keys()):
                raise NotImplementedError('Unknown Chemistry Network')
            elif not set(self.__species).issubset(set(ch_i5)):
                network = 15
            else:
                try:
                    add = lambda x, y: x + y
                    isum = reduce(add, [self.__ffile.read_plotvar(ch_ikey[key])[0,0,0,0] for key in ch_i5])
                    if isum < .99:
                        network = 15
                    else:
                        network = 5
                except:
                    raise Exception('Could Not Detect Chemistry Network Type')            
            #print 'No network provided. Guessed type %i.' % network
        self.__network = network
        # (Read species mass fractions) Set aside until needed
        self.__idict = {}
        # (Calculate cell volumina for mass calculations) Set aside until needed
        self.__cellvol = None
        # Read relative nucleeonic abundances of inert species:
        self.__abundhe = ffile.read_param('real runtime parameters', 'ch_abundhe')
        self.__abundc  = ffile.read_param('real runtime parameters', 'ch_abundc')
        self.__abundo  = ffile.read_param('real runtime parameters', 'ch_abundo')
        self.__abundsi = ffile.read_param('real runtime parameters', 'ch_abundsi')
        #        
        if network == 5:
            self.__ch_mu = ch_mu5
            # Renormalization: Count total nucleons per actual nucleons of the species;
            # thus adding (relative) nucleons of inert species
            # per nucleons of actual species:
            # ch_mH ignored in 1st line: See Simulation_initSpecies.F90 # Strange
            self.__abar  = self.__ch_mu['Ha']
            self.__abar += self.__ch_mu['He'] * self.__abundhe
            self.__abar += self.__ch_mu['C']  * self.__abundc
            self.__abar += self.__ch_mu['O']  * self.__abundo
            self.__abar += self.__ch_mu['Si'] * self.__abundsi
            self.__abar *= (1.-2.331099164809558e-7)
        elif network == 15:
            self.__ch_mu = ch_mu15
            self.__abar  = 1.0
        elif network == 'eduroam':
            raise Exception('Go Home, You\'re Drunk')
        else:
            raise NotImplementedError('Unknown Chemistry Network')
    def get_species(self, include_inert=False):
        '''
        returns a list of those species whose statistics are available
        through the data contained in the plotfile
        '''
        answer = self.__species
        if self.__network == 5 and include_inert:
            if self.__abundhe > 0.:
                answer.append('He')
            if self.__abundc  > 0.:
                answer.append('C')
            if self.__abundo  > 0.:
                answer.append('O')
            if self.__abundsi > 0.:
                answer.append('Si')
        return answer
    def get_network(self):
        return self.__network
    def __repr__(self, level=0):
        answer  = 'Flashfile Chemistry Data' + '\n'
        answer  = self.__ffile.filename() + '\n'
        answer += 'Network Type: ' +str(self.__network) + '\n'
        answer += 'Known Network Species: ' + self.__species[0]
        for species in self.__species[1:]:
            answer += ', ' + species
        answer += '\n'
        answer += 'Mass Renormalization Factor: %.9f'%(self.__abar) + '\n'
        return answer
    def i(self, species=None):
        '''
        calculates the mass fraction of the specified species
        '''
        if species in self.__species:
            i_species = self.__ffile.read_plotvar(ch_ikey[species], sanitize=False)
            # Account for the 5-species network only counting the C-atoms
            # of CO towards the mass fraction
            if  self.__network == 5 and species == 'CO':
                i_species *= self.__ch_mu['CO'] / self.__ch_mu['Cp']
            return i_species / self.__abar 
        elif self.__network == 5:
            if species == 'He':
                return self.__ch_mu['He'] * self.__abundhe
            elif species == 'C':
                return self.__ch_mu['C']  * self.__abundc
            elif species == 'O':
                i_O = self.__ch_mu['O']  * self.__abundo
                # TODO: insert correction subtracting those O-atoms bound in CO
                # (Problem: What to do if species CO not plotfile available?)
                return i_O
            elif species == 'Si':
                return self.__ch_mu['Si'] * self.__abundsi
            else:
                raise KeyError('Unknown Species %s' % species)
        elif self.__network == 15:
            if False:
                pass
            else:
                raise KeyError('Unknown Species %s' % species)
        else:
            raise KeyError('Unknown Species %s' % species)
    def dens(self, species=None):
        '''
        calculates the (mass) density of the specified species
        (mass units per unit volume element).
        '''
        if species is None or species in ('total', ''):
            return self.__dens
        else:
            return self.__dens * self.i(species)
    def numdens(self, species=None):
        '''
        calculates the particle number density of the specified species
        (number of molecules per unit volume element).
        '''
        if species is None or species in ('total', ''):
            temp = self.__ffile.read_plotvar('temp')
            pres = self.__ffile.read_plotvar('pres')
            return pres/(temp*k_b)
        else:
            return self.dens(species) / (self.__ch_mu[species]*m_a)
    def n(self, species=None):
        '''
        calculates the density of the specified species in terms of
        the number density of hydrogen nuclei of equivalent mass density
        (~number of nucleons per unit volume element).
        '''
        return self.dens(species) / (self.__ch_mu['Ha']*m_a)
    def abund(self, species=None):
        '''
        calculates the fractional abundance of the specified species,
        defined as the particle number density of the species divided
        by the overall equivalent hydrogen number density
        '''
        return self.numdens(species) / self.n()
    def __eval_cellvol(self):
        if self.__cellvol is None:
            nxb = self.__ffile.read_param('integer scalars', 'nxb')
            nyb = self.__ffile.read_param('integer scalars', 'nyb')
            nzb = self.__ffile.read_param('integer scalars', 'nzb')
            blocksize = self.__ffile.read_plotvar('block size')
            self.__cellvol = (np.product(blocksize,axis=1)*(1./(nxb*nyb*nzb))).reshape(-1,1,1,1)
    def vol(self):
        '''
        returns the volume contained in each simulation cell
        (block data)
        '''
        self.__eval_cellvol()
        return np.ones_like(self.__dens) * self.__cellvol
    def mass(self, species=None):
        '''
        returns the mass of a specific species contained in each simulation cell
        (block data)
        '''
        self.__eval_cellvol()
        return self.dens(species) * self.vol()
    def numcount(self, species=None):
        '''
        returns the total number of a specific species molecules
        contained in each simulation cell
        (block data)
        '''
        self.__eval_cellvol()
        return self.numdens(species) * self.vol()
    def ncount(self, species=None):
        '''
        returns the total number of a specific species mass equivalent number
        of hydrogen atoms contained in each simulation cell
        (block data)
        '''
        self.__eval_cellvol()
        return self.n(species) * self.vol()
        
# ==== CLASS: flash file data grid =============================================
class ffgrid(object):
    def __init__(self, flashfile, verbose=True):
        global _verbose_
        _verbose_ = verbose
        verboseprint('Reading Layout Data...')
        block_bbox = flashfile.read_plotvar('bounding box')
        nodetype = flashfile.read_plotvar('node type').astype(np.uint64)
        self.__filename = flashfile.filename()
        verboseprint('Processing Layout Data...')
        self.__nxb = flashfile.read_param('integer scalars', 'nxb')
        self.__nyb = flashfile.read_param('integer scalars', 'nyb')
        self.__nzb = flashfile.read_param('integer scalars', 'nzb')
        self._nb = (self.__nxb, self.__nyb, self.__nzb)
        self._blockcells = self.__nxb*self.__nyb*self.__nzb
        self._bbox_min = block_bbox.min(axis=2)
        self.__block_xmin = self._bbox_min[:,0]
        self.__block_ymin = self._bbox_min[:,1]
        self.__block_zmin = self._bbox_min[:,2]
        self._bbox_max = block_bbox.max(axis=2)
        self._bbox_center = .5*(self._bbox_min+self._bbox_max)
        self._bbox_size = self._bbox_max - self._bbox_min
        self._bbox_minsize = self._bbox_size.min(axis=0)
        self._domain_min = self._bbox_min.min(axis=0)
        self._domain_max = self._bbox_max.max(axis=0)
        self._domain_size = self._domain_max - self._domain_min
        self._domain_blocks = (self._domain_size / self._bbox_minsize +.5).astype(np.int32)
        self._domain_cells = self._domain_blocks * np.array(self._nb)
        ### Build a block index tree indicating the spacial block layout
        verboseprint('Building Block Index Tree...')
        blocktree.Build(block_bbox, nodetype)
        ### Build a cell index matrix indicating the spacial cell layout per block
        verboseprint('Building Cell Index Matrix...')
        self._I_cell = np.ones(self._nb, dtype=np.int32) * -1
        for i in xrange(self.__nxb):
            for j in xrange(self.__nyb):
                for k in xrange(self.__nzb):
                    self._I_cell[i,j,k] = i + self.__nxb*j + self.__nxb*self.__nyb*k
        # Precalculate (reciprocal) values to aleviate findblockcell
        self.__cell_xsize_recp = self.__nxb * 1./self._bbox_size[:,0]
        self.__cell_ysize_recp = self.__nyb * 1./self._bbox_size[:,1]
        self.__cell_zsize_recp = self.__nzb * 1./self._bbox_size[:,2]
        ### Build a kd-tree according to a flattened list of the coordinates
        self.__cell_coords = None # postpone construction till needed
        self.__cellbox_min = None # postpone construction till needed
        self.__cellbox_max = None # postpone construction till needed
        self.__tree = None # postpone construction till needed
        
    def __build_cell_coords(self):
        if self.__cell_coords is None:
            ### Calculate coordinates of the cells inside the blocks
            x_reloffset = (np.linspace(-.5, .5, self.__nxb, endpoint=False) + .5/self.__nxb)
            y_reloffset = (np.linspace(-.5, .5, self.__nyb, endpoint=False) + .5/self.__nyb)
            z_reloffset = (np.linspace(-.5, .5, self.__nzb, endpoint=False) + .5/self.__nzb)
            reloffset = np.array(np.meshgrid(y_reloffset, z_reloffset, x_reloffset))[(2,0,1),:,:,:]
            offset = (self._bbox_size).reshape(-1,3,1,1,1)*reloffset
            self.__cell_coords = (self._bbox_center).reshape(-1,3,1,1,1) +offset
            cellbox_radius = (.5*self._bbox_size/self._nb).reshape(-1,3,1,1,1)
            self.__cellbox_min = self.__cell_coords - cellbox_radius
            self.__cellbox_max = self.__cell_coords + cellbox_radius
        else:
            pass
            
    def __build_tree(self):
        """
        Build cell level btree of grid nodes.
        This is only used for neighborhood search of some interpolation algorithms.
        """
        self.__build_cell_coords()
        # TODO: Implement consideration of boundary conditions in tree topology!
        # (Search tree ignores periodic bc. in current version -> bad interpolation)
        if self.__tree is None:
            ### Build a kd-tree according to a flattened list of the coordinates
            ### of alls cells centers to provide a means for neighborhood search.
            # This will typically be used to support some interpolation algorithms.
            coords_flat = self.__cell_coords.swapaxes(0,1).reshape(3,-1).T
            self.__tree = cKDTree(coords_flat, balanced_tree=False)
        else:
            pass
            
    def __repr__(self, level=0):
        answer  = 'Flashfile 3D AMR Grid' + '\n'
        answer += self.__filename + '\n'
        answer += 'Block layout:  ' + str(self._domain_blocks) + ' '
        answer += '(' + str(len(self._bbox_min)) + ')' + '\n'
        answer += 'Block celling: ' + str(np.array(self._nb)) + '\n'
        answer += 'Cell layout:   ' + str(self._domain_cells) + ' '
        answer += '(' + str(len(self._bbox_center)*self._blockcells) + ')' + '\n'
        return answer
    def findblock(self, x, y, z):
        """
        Return the index of the block containing the coordinates x, y, z.
        Capable of processing multiple data on receiving vectorized input.
        """
        return blocktree.Findblock(x, y, z)
    def findblockcell(self, block, x, y, z):
        """
        Return the index of the blocks cell containing the coordinates x, y, z.
        Make sure that the provided block actually containes the coordinates.
        If the correct block index is unknown, findblock should be used in advance.
        Capable of processing multiple data on receiving vectorized input.
        """
        ix = ((x-self.__block_xmin[block])*self.__cell_xsize_recp[block]).astype(np.int32)
        iy = ((y-self.__block_ymin[block])*self.__cell_ysize_recp[block]).astype(np.int32)
        iz = ((z-self.__block_zmin[block])*self.__cell_zsize_recp[block]).astype(np.int32)
        return self._I_cell[ix,iy,iz]
    def findcell(self, x, y, z):
        """
        Return the index of the block and the flat index of the blockcell
        containing the coordinates x, y, z.
        Capable of processing multiple data on receiving vectorized input.
        """
        block = self.findblock(x, y, z)
        return block, self.findblockcell(block, x, y, z)
    def findneighborhood(self, X, Y, Z, k=1, eps=0.01):
        """
        Query the grid for nearest neighbors.
        Return the indicies of the blocks and the flat indicies of the blockcells
        closest to the coordinates x, y, z, and the distance to those cells.
        Capable of processing multiple data on receiving vectorized input.
        """
        X, Y, Z = np.array(X), np.array(Y), np.array(Z)
        if k==1:
            baseshape = X.shape
        else:
            baseshape = X.shape+(k,)
        self.build_tree()
        D,I = np.squeeze(
            self.__tree.query(np.vstack((X.flatten(),Y.flatten(),Z.flatten())).T, k=k, eps=eps,
            p=2, distance_upper_bound=np.inf, n_jobs=-1))
        # Factorize the flattened coordinate array indicies
        # back into block and cell indicies.
        block = np.floor_divide(I, self._blockcells).astype(np.int32)
        cell = np.mod(I, self._blockcells).astype(np.int32)
        return block.reshape(baseshape), cell.reshape(baseshape), D.reshape(baseshape)
        
# --- SPATIAL BASEGRID ---------------------------------------------------------
    def resolution(self, axis=None):
        if axis is None:
            return self._domain_cells
        elif axis in range(3):
            return self._domain_cells[axis]
        else:
            raise ValueError('Axis out of range.')
            
    def spacing(self, axis=None, res=None):
        if axis is None:
            if res is None:
                res = self._domain_cells
            if res[0] is None:
                res[0] = self._domain_cells[0]
            if res[1] is None:
                res[1] = self._domain_cells[1]
            if res[2] is None:
                res[2] = self._domain_cells[2]
            spacing = self._domain_size/res
        elif axis in range(3):
            if res is None:
                res = self._domain_cells[axis]
            spacing = self._domain_size[axis]/res
        else:
            raise ValueError('Axis out of range.')
        return spacing
        
    def grating(self, axis, res=None, extent=None):
        """
        Partition the given axis of the given extend / entire simulation domain,
        equally into res parts (return the center coordinates of those parts).
        If none resolution is provided, the resolution will correspond to the
        best grid refinements resolution, with the coordinates in the centers
        of those grids cells.
        Typically used to construct grids for sampling points in the sim space.
        """
        if extent is None:
            grating_min = self._domain_min[axis]
            grating_max = self._domain_max[axis]
        else:
            grating_min, grating_max = extent

        if res is None:
            step = self.spacing(axis)
        else:
            step = (grating_max-grating_min)/res
        
        d_domain = self._domain_max[axis]-self._domain_min[axis]
        res_domain = d_domain/step +.5
        first = self._domain_min[axis] + .5*step
        last = self._domain_max[axis] - .5*step
        grating_domain = np.linspace(first, last, res_domain)
        grating = rangeselect(grating_domain, grating_min, grating_max)
        return grating
            
    def basegrid_2d(self, axis, xres=None, yres=None, zres=None,
        x_extent=None, y_extent=None, z_extent=None):
        if axis == 0:
            z_grating = self.grating(2, zres, z_extent)
            y_grating = self.grating(1, yres, y_extent)
            return np.meshgrid(z_grating, y_grating)
        elif axis == 1:
            z_grating = self.grating(2, zres, z_extent)
            x_grating = self.grating(0, xres, x_extent)
            return np.meshgrid(z_grating, x_grating)
        elif axis == 2:
            y_grating = self.grating(1, yres, y_extent)
            x_grating = self.grating(0, xres, x_extent)
            return np.meshgrid(y_grating, x_grating)
        else:
            raise ValueError('Axis out of range.')

    def basegrid_3d(self, xres=None, yres=None, zres=None,
        x_extent=None, y_extent=None, z_extent=None):
        #
        y_grating = self.grating(1, yres, y_extent)
        x_grating = self.grating(0, xres, x_extent)
        z_grating = self.grating(2, zres, z_extent)
        return np.meshgrid(y_grating, x_grating, z_grating)

    def varblockgrating(self, axis):
        if axis not in range(3):
            raise ValueError('Axis %i out of range(3).' % axis)
        blockbounds = np.concatenate((self._bbox_min[:,axis], self._bbox_max[:,axis]))
        II = (blockbounds-self._domain_min[axis])/self._bbox_minsize[axis]
        Ipartition = np.sort(list(set(np.around(II.T))))    
        IMid = .5*(Ipartition[1:]+Ipartition[:-1])
        IWidth = Ipartition[1:]-Ipartition[:-1]    
        grating = self._domain_min[axis] + IMid*self._bbox_minsize[axis]
        return grating, IWidth

    def varcellgrating(self, axis, extent=None):
        if axis not in range(3):
            raise ValueError('Axis %i out of range(3).' % axis)
        self.__build_cell_coords()
        cellbounds = np.concatenate((
            self.__cellbox_min[:,axis].flatten(),
            self.__cellbox_max[:,axis].flatten(), ))
        II = (cellbounds-self._domain_min[axis])/self.spacing(axis=axis)
        Ipartition = np.sort(list(set(np.around(II.T))))    
        IMid = .5*(Ipartition[1:]+Ipartition[:-1])
        IWidth = Ipartition[1:]-Ipartition[:-1]    
        grating = self._domain_min[axis] + IMid*self.spacing(axis=axis)
        if extent is None:
            return grating, IWidth
        else:
            grating_min, grating_max = extent
            mask = (grating>=grating_min)*(grating<grating_max)
            return grating[mask], IWidth[mask]

# --- DATA AQUISITON -----------------------------------------------------------
    def eval_scalar(self, file_data, X, Y, Z, interpolation=None, p=None):
        """
        Evaluate the provided scalar block dataset for a given position.
        Capable of processing multiple data on receiving vectorized X,Y,Z input.
        This will typically be components of a grid.
        Caution: X,Y,Z need to have identical shapes;
        if using a 2d-grid, fill the remaining variable accordingly,
        which can be done by numpy.full_like.
        """
        flatcell_shape = list(file_data.shape[:-3])+[-1]
        if interpolation is None:
            # Do no interpolation, but directly use data from cell
            # containing the provided coordinates.
            block, cell = self.findcell(X,Y,Z)
            return file_data.reshape(flatcell_shape)[block,cell]
        elif interpolation == 'none':
            # Do no interpolation, but directly use data from cell
            # containing the provided coordinates.
            block, cell = self.findcell(X,Y,Z)
            return file_data.reshape(flatcell_shape)[block,cell]            
        elif interpolation == 'nearest':
            # Interpolate by taking value of cell with nearest center.
            block, cell, D = self.findneighborhood(X,Y,Z)
            return file_data.reshape(flatcell_shape)[block,cell]
        elif interpolation == 'idw1':
            # idw as above, but with exponent p=1,
            # exponentiation removed for faster computation.
            block, cell, D = self.findneighborhood(X,Y,Z, k=9)
            Dmax = (D.max(axis=-1)).T
            Dmin = 1e-6*Dmax
            Weight = (((Dmax-D.T)*np.reciprocal(Dmax*D.T+Dmin)).T)
            Data = file_data.reshape(flatcell_shape)[block,cell]
            return (Data*Weight).sum(axis=-1) / Weight.sum(axis=-1)
        elif interpolation == 'idw':
            if p is None: p=2 # standard idw exponent
            # Interpolate by inverse distance weighting.
            # To operate safely, idw needs at least k=9 neighboring points:
            # If the selected X,Y,Z exactly mark the center of a cubical cell,
            # the nearest 8 points are the corners of that tube,
            # each having the same distance to the query point,
            # which for k<9 would then lead to all weights being equally nought.
            block, cell, D = self.findneighborhood(X,Y,Z, k=9)
            Dmax = (D.max(axis=-1)).T
            Dmin = 1e-6*Dmax
            Weight = (((Dmax-D.T)*np.reciprocal(Dmax*D.T+Dmin)).T)**p
            Data = file_data.reshape(flatcell_shape)[block,cell]
            return (Data*Weight).sum(axis=-1) / Weight.sum(axis=-1)
        elif interpolation == 'smooth':
            # Do not interpolate, but rather use a kernel smoother.
            # (The difference being that the smoother is not guaranteed to
            #  evaluate to actual grid values in places where they are defined)
            # This works similar to idw, but uses a finite kernel
            block, cell, D = self.findneighborhood(X,Y,Z, k=17)
            Dmax = (D.max(axis=-1)).T
            Weight = (1.01-(D.T/Dmax).T)**2
            Data = file_data.reshape(flatcell_shape)[block,cell]
            return (Data*Weight).sum(axis=-1) / Weight.sum(axis=-1)
        else:
            # Interpolation parameter setting unrecognized
            print ('Warning: Unrecognized interpolation mode \''
            +str(interpolation) + '\', using \'none\' instead.')
            block, cell = self.findcell(X,Y,Z)
            return file_data.reshape(flatcell_shape)[block,cell]
            
    def equalize_blockaxis(self, data, axis, weights=None):
        if axis not in range(3):
            raise ValueError('Axis %i out of range(3).' % axis)
        d_axis = -(axis+1)
        E = np.ones(self._nb, dtype=np.float64)
        if weights is None:
            data_eq = np.expand_dims(
                data.mean(axis=d_axis), d_axis  ) * E
            weights_eq = None
        else:
            weights_eq = np.expand_dims(
                weights.mean(axis=d_axis), d_axis  ) * E
            data_eq = np.expand_dims(
                (data*weights).mean(axis=d_axis), d_axis  ) * E / weights_eq
        return data_eq, weights_eq

# --- DATA AQUISITON HELPERS FOR COMMON TASKS ----------------------------------        
    def read_slice(self, file_data, axis, intercept=0.,
        interpolation=None, xres=None, yres=None, zres=None,
        x_extent=None, y_extent=None, z_extent=None):
        """
        Reads a slice perpendicular to axis from the given flashfile data
        at the given axis intercept,
        e.g. axis=0 -> X, intercept=4e+19 => read slice at X=4e+19.
        If no resolution is provided, the finest native grid resolution
        of the actual grid of the flashfile is assumed.
        The extent of the processed area can be limited by providing
        e.g. x_extent=(x_min, x_max) for any axis.
        TODO: At this stage, the resolution refers to the entire unlimited domain.
        """
        if axis==0:
            Z, Y = self.basegrid_2d(axis, xres=xres, yres=yres, zres=zres,
                x_extent=x_extent, y_extent=y_extent, z_extent=z_extent)
            X = np.full_like(Z, intercept)
        elif axis==1:
            Z, X = self.basegrid_2d(axis, xres=xres, yres=yres, zres=zres,
                x_extent=x_extent, y_extent=y_extent, z_extent=z_extent)
            Y = np.full_like(Z, intercept)
        elif axis==2:
            Y, X = self.basegrid_2d(axis, xres=xres, yres=yres, zres=zres,
                x_extent=x_extent, y_extent=y_extent, z_extent=z_extent)
            Z = np.full_like(Y, intercept)
        else:
            raise ValueError('Axis out of range(3).' % axis)
        return self.eval_scalar(file_data, X, Y, Z, interpolation=interpolation)
        
    def read_meanslice(self, file_data, axis, weight_data=None,
        interpolation=None, xres=None, yres=None, zres=None, verbose=None,
        x_extent=None, y_extent=None, z_extent=None):
        """
        Reads a slice of weighted mean values, calculated along the given axis.
        from the given flashfile data and weights from flashfile data.
        If no weights data is provided, equal weights are assumed.
        If no resolution is provided, the finest native grid resolution
        of the actual grid of the flashfile is assumed.
        The resolution parameter of the (integrated) axis sets an upper
        limit for the depth scanning resolution.
        The extent of the processed area can be limited by providing
        e.g. x_extent=(x_min, x_max) for any axis.
        TODO: For the time being, the extend along the integrated axis
        can not be limited. This is hard to solve because of the depth-scanning
        algorithm not depth scanning at cell level.
        """
        global _verbose_
        if verbose is not None:
            _verbose_ = verbose 
        # Build 2d-slice grid for sampling multiple slices perpendicular
        # to the given axis
        if axis==0:
            Z, Y = self.basegrid_2d(axis, xres=xres, yres=yres, zres=zres,
                x_extent=x_extent, y_extent=y_extent, z_extent=z_extent)
            X = None
        elif axis==1:
            Z, X = self.basegrid_2d(axis, xres=xres, yres=yres, zres=zres,
                x_extent=x_extent, y_extent=y_extent, z_extent=z_extent)
            Y = None
        elif axis==2:
            Y, X = self.basegrid_2d(axis, xres=xres, yres=yres, zres=zres,
                x_extent=x_extent, y_extent=y_extent, z_extent=z_extent)
            Z = None
        else:
            raise ValueError('Axis %i out of range(3).' % axis)
        
        axis_extent = (x_extent, y_extent, z_extent)[axis]
        if axis_extent is None:
            ### The situation is simple. Do massive optimizations.
            # Accelerate grid scanning (1):
            # Equalize cell data in blocks along depth axis,
            # so that the depth scanning has to resolve the smallest block length only
            # instead of the smallest cell length:
            data_eq, weigths_eq = self.equalize_blockaxis(file_data, axis, weight_data)
            # Accelerate grid scanning (2):
            # Instead scanning the full depth axis using the smallest block size,
            # scan only the midpoints of the finest resolution blocks 
            # covering the axis. (weigh the results accordingly)
            grating, gratingweights = self.varblockgrating(axis)
        else:
            ### The situation is less simple, I don't know how to do all of
            ### above optimizations.
            # Accelerate grid scanning (1):
            # Instead scanning the full depth axis using the smallest grid size,
            # scan only the midpoints of the finest resolution cells 
            # covering the axis. (weigh the results accordingly)
            data_eq, weigths_eq = file_data, weight_data
            grating, gratingweights = self.varcellgrating(axis, axis_extent)
        if verbose:
            answer  = str(len(grating))
            answer += (axis_extent is None)*('*'+str(self._nb[axis]))
            answer += ' / ' +str(self._domain_blocks[axis]*self._nb[axis]) +' (var)'
            verboseprint('Depth resolution:', answer)
            
        if weight_data is None:    
            result_accu = np.zeros_like((X,Y,Z)[axis-1])   
            for intercept, iweight in izip(grating, gratingweights):
                if axis==0:
                    X = np.full_like(Z, intercept)
                elif axis==1:
                    Y = np.full_like(Z, intercept)
                elif axis==2:
                    Z = np.full_like(Y, intercept)
                dataslice = self.eval_scalar(data_eq, X, Y, Z, interpolation=interpolation)
                result_accu += dataslice*iweight
            return result_accu*(1./gratingweights.sum())
        else: # if weight_data is a thing:
            result_accu = np.zeros_like((X,Y,Z)[axis-1])   
            weight_accu = np.zeros_like((X,Y,Z)[axis-1])   
            for intercept, iweight in izip(grating, gratingweights):
                if axis==0:
                    X = np.full_like(Z, intercept)
                elif axis==1:
                    Y = np.full_like(Z, intercept)
                elif axis==2:
                    Z = np.full_like(Y, intercept)
                dataslice = self.eval_scalar(data_eq, X, Y, Z, interpolation=interpolation)
                weightslice = self.eval_scalar(weigths_eq, X, Y, Z, interpolation=interpolation)
                result_accu += dataslice*weightslice*iweight
                weight_accu += weightslice*iweight
            return result_accu/weight_accu
    
    def read_column(self, file_data, axis, x=0., y=0., z=0.,
        interpolation=None, xres=None, yres=None, zres=None):
        if axis not in range(3):
            raise ValueError('Axis %i out of range(3).' % axis)
        axisres = (xres, yres, zres)[axis]
        grating = self.grating(axis, axisres)
        if axis==0:
            Y = np.full_like(grating, y)
            Z = np.full_like(grating, z)
            data_line = self.eval_scalar(file_data, grating, Y, Z,
                interpolation=interpolation)
        elif axis==1:
            X = np.full_like(grating, x)
            Z = np.full_like(grating, z)
            data_line = self.eval_scalar(file_data, X, grating, Z,
                interpolation=interpolation)
        else: # axis==2
            X = np.full_like(grating, x)
            Y = np.full_like(grating, y)
            data_line = self.eval_scalar(file_data, X, Y, grating,
                interpolation=interpolation)
        return grating, data_line
        
    def read_meancolumn(self, file_data, axis, weight_data=None,
        interpolation=None, xres=None, yres=None, zres=None):
        # NEEDS WORK: RECREATES MESHGRID FOR EVERY SLIDE.
        # SHOULD DO IT LIKE IN MEANSLICE INSTEAD.
        if axis not in range(3):
            raise ValueError('Axis %i out of range(3).' % axis)
        # Limit resolution along integration dirs to actual grid resolution.
        # 2nd line assumes the axis _res parameter to be ignored after that.
        axisres = (xres, yres, zres)[axis]
        xres, yres, zres = np.minimum((xres, yres, zres), self.resolution())
        #
        grating = self.grating(axis, axisres)
        result_line = np.zeros_like(grating)   
        if weight_data is None:    
            for i, intercept in enumerate(grating):
                dataslice = self.read_slice(file_data, axis, intercept,
                    interpolation=interpolation, xres=xres, yres=yres, zres=zres)
                result_line[i] = dataslice.mean()
        else:
            for i, intercept in enumerate(grating):
                dataslice = self.read_slice(file_data, axis, intercept,
                    interpolation=interpolation, xres=xres, yres=yres, zres=zres)
                weightslice = self.read_slice(weight_data, axis, intercept,
                    interpolation=interpolation, xres=xres, yres=yres, zres=zres)
                result_line[i] = (dataslice*weightslice).sum()/weightslice.sum()
        return grating, result_line
        
    def read_grid(self, file_data, xres=None, yres=None, zres=None, interpolation=None):
        if interpolation is None:
            xres, yres, zres = np.minimum((xres, yres, zres), self.resolution())
        Y, X, Z = self.basegrid_3d(xres=xres, yres=yres, zres=zres)
        grid_data = self.eval_scalar(file_data, X, Y, Z, interpolation=interpolation)
        return grid_data
        
    def rot_grid(self, data_x, data_y, data_z, xres=None, yres=None, zres=None, interpolation='idw1'):
        if interpolation is None:
            xres, yres, zres = np.minimum((xres, yres, zres), self.resolution())
        datax_grid = self.read_grid(data_x, interpolation=interpolation,
            xres=xres, yres=yres, zres=zres)
        datay_grid = self.read_grid(data_y, interpolation=interpolation,
            xres=xres, yres=yres, zres=zres)
        dataz_grid = self.read_grid(data_z, interpolation=interpolation,
            xres=xres, yres=yres, zres=zres)
        dx, dy, dz = self.spacing(res=np.array((xres, yres, zres)))        
        datax_grad = np.gradient(datax_grid, dx, dy, dz)
        datay_grad = np.gradient(datay_grid, dx, dy, dz)
        dataz_grad = np.gradient(dataz_grid, dx, dy, dz)
        data_rotx = dataz_grad[1] -datay_grad[2]
        data_roty = datax_grad[2] -dataz_grad[0]
        data_rotz = datay_grad[0] -datax_grad[1]
        return np.array((data_rotx, data_roty, data_rotz))
