# -*- coding: utf-8 -*-
from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy

ext_module = Extension(
    "blocktree",
    ["cppblocktree.cpp", "blocktree.pyx"],
    language='c++',
    include_dirs=[numpy.get_include()],
    extra_compile_args=['-std=c++0x', '-fopenmp'],
    extra_link_args=['-fopenmp',],
)

setup(
    name = 'Hello world app',
    cmdclass = {'build_ext': build_ext},
    ext_modules = [ext_module],
)
